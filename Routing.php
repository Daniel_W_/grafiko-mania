<?php

require_once 'Controllers\SecurityController.php';
require_once 'Controllers\MainController.php';
require_once 'Controllers\ScheduleController.php';
require_once 'Controllers\CreateController.php';
require_once 'Controllers\AccountController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'mainpage' => [
                'controller' => 'MainController',
                'action' => 'showMenu'
            ],
            'schedule' => [
                'controller' => 'ScheduleController',
                'action' => 'showSchedule'
            ],
            'task' => [
                'controller' => 'ScheduleController',
                'action' => 'showTask'
            ],
            'stats' => [
                'controller' => 'ScheduleController',
                'action' => 'showStats'
            ],
            'create' => [
                'controller' => 'CreateController',
                'action' => 'addTask'
            ],
            'assign' => [
                'controller' => 'CreateController',
                'action' => 'assignSchedule'
            ],
            'account' => [
                'controller' => 'AccountController',
                'action' => 'showAccount'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if(isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}

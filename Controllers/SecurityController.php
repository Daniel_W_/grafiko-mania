<?php

require_once 'AppController.php';
require_once __DIR__.'/../Models/User.php';

class SecurityController extends AppController {

    public function login()
    {
        $user1 = new User('adrian@pkedu.pl', 'admin', 'Adi', 'Kowal');
        //$user1 = mew User('adrian@pkedu.pl', 'admin', 'Adi', 'Kowal');

        //$users= [$user1->getEmail(), $user1->getPassword()];
        if($this->isPost()){
            $email = $_POST['email'];
            $password = $_POST['password'];


            if (!$user1) {
                $this->render('login', ['messages' => ['User with this email not exist!']]);
                return;
            }

            if ($user1->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }
            header("Location: http://$_SERVER[HTTP_HOST]/grafiko-mania/?page=mainpage");
            return;
        }
        $this->render('login');
    }

    public function register()
    {
        //dodawanie uzytkownika do bazy
       // header("Location: http://$_SERVER[HTTP_HOST]/grafiko-mania/?page=register");
        $this->render('register');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['You have been successfully logged out!']]);
    }
}
<?php

require_once 'AppController.php';

class ScheduleController extends AppController {
    public function showSchedule()
    {
        $this->render('schedule');
    }
    public function showTask()
    {
        $this->render('task');
    }
    public function showStats()
    {
        $this->render('stats');
    }
}

<?php

require_once 'AppController.php';

class MainController extends AppController {
    public function showMenu()
    {
        $this->render('mainpage');
    }
}
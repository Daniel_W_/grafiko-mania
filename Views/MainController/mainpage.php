<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/mainpage.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <a href="?page=account" class="account"><img src="Public/img/account.svg" alt="Twoje Konto"/></a>
    <a href="?page=schedule" ><button type="button" class="main-btn">Grafik na dziś</button></a>
    <a href="?page=create" ><button type="button" class="main-btn">Utwórz grafik</button></a>
    <a href="?page=tip" ><button type="button" class="main-btn">Porada dnia</button></a>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/schedule.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <div class="content">
        <h1 class="task">Zadanie 1<a href="?page=task"><button type="button" class="play"><i class="fas fa-play fa-1.5x"></i></button></a></h1>
        <h1 class="task">Zadanie 1<a href="?page=task"><button type="button" class="play"><i class="fas fa-play fa-1.5x"></i></button></a></h1>
        <h1 class="task">Zadanie 1<a href="?page=task"><button type="button" class="play"><i class="fas fa-play fa-1.5x"></i></button></a></h1>
        <h1 class="task">Zadanie 1<a href="?page=task"><button type="button" class="play"><i class="fas fa-play fa-1.5x"></i></button></a></h1>
    </div>
    <a href="?page=stats"><button type="button" class="end-btn">Zakończ</button></a>
</div>
</body>
</html>

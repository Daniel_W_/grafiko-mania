<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/stats.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <a href="?page=account" class="account"><img src="Public/img/account.svg" alt="Twoje Konto"/></a>
    <h1>STATYSTYKI</h1>
    <div class="content">
        <h3>Wykonane zadania:</h3>
        <h2>3/3</h2>
        <h3>Najszybciej wykonane zadanie:</h3>
        <h2>Zadanie 3 - 10min</h2>
        <h3>Najwolniej wykonane zadnie</h3>
        <h2>Zadanie 1 - 1h</h2>
    </div>
</div>
</body>
</html>


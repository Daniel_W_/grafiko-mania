<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/task.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <div class="name">Zadanie 1</div>
    <div class="content">Opis zadania...</div>
    <a href="?page=schedule"><button type="button" class="stop-btn"><i class="fas fa-stop fa-5x"></i></button></a>
    <a href="?page=schedule"><button type="button" class="finish-btn"><i class="fas fa-check fa-6x"></i></button></a>
</div>
</body>
</html>


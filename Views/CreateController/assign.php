<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/assign.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <h1>UTWORZONY GRAFIK:</h1>
    <div class="content">
        <!-- dodac zadania z bazy -->
    </div>
    <!-- dodac kalendarz z mozliwoscia wyboru daty -->
    <form action = "?page=create" method = "POST">
        <select name="nazwa">
            <!-- wypisac uzytkownikow z tej samej grupy -->
            <option>Tu wpisz pierwszą możliwość</option>
            <option>Tu wpisz drugą możliwość</option>
        </select>
        <button type="submit" class="confirm-btn">Zatwierdź</button>
        <a href="?page=mainpage" ><button type="button" class="discard-btn">Odrzuć</button></a>
    </form>
</div>
</body>
</html>


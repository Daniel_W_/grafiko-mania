<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/create.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <a href="?page=mainpage" class="logo"><img src="Public/img/logo_small.svg" alt="Grafiko-Mania"/></a>
    <form action = "?page=create" method = "POST">
        <input class="name" name="task" type="text" placeholder="Nazwa zadania">
        <input class="content" name="description" type="text" placeholder="Opis zadania">
        <div class="row">
            <div class="btn-group-vertical hours">
                <ul class="wcag-flat">
                    <li>
                        <button type="button" class="left btn btn-time-up">
                            <i class="far fa-arrow-alt-circle-up"></i>
                        </button>
                    </li>
                    <li>
                        <button type="button" class="left btn btn-down-up">
                            <i class="far fa-arrow-alt-circle-down"></i>
                        </button>
                    </li>
                </ul>
                <!-- /.wcag-flat -->
            </div><!-- btn-group-vertical hours -->
            <div class="data-field">
                <fieldset>
                    <label for="hour" class="wcag-hide">Wpisz godzinę <span>Format GG:MM</span></label>
                    <input id="hour" type="text" class="day-hour form-control" placeholder="" style="text-align: right;"><!-- day-hour -->
                </fieldset>
            </div><!-- data-field -->
            <div class="btn-group-vertical minutes">
                <ul class="wcag-flat">
                    <li>
                        <button type="button" class="left btn btn-time-up">
                            <i class="far fa-arrow-alt-circle-up"></i>
                        </button>
                    </li>
                    <li>
                        <button type="button" class="left btn btn-down-up">
                            <i class="far fa-arrow-alt-circle-down"></i>
                        </button>
                    </li>
                </ul>
                <!-- /.wcag-flat -->
            </div><!-- btn-group-vertical minutes -->
        </div>
        <button type="submit" class="create-btn">Dodaj</button>
        <a href="?page=assign" ><button type="button" class="assign-btn">Zakończ</button></a>
    </form>
</div>
</body>
</html>
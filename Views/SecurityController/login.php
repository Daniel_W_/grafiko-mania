<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/login.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <img class="center" src="Public/img/logo_big.svg" alt="Grafiko-Mania"/>
    <div class="loginbox">
        <form action = "?page=login" method = "POST">
            <div class = 'messages'>
             <?php
                if(isset($messages)) {
                    foreach($messages as $message){
                        echo $message;
                    }
                }
                ?>
            </div>
            <input name="email" type="text" placeholder="email@adres.com">
            <input name="password" type="password" placeholder="hasło">
            <button type="submit" class="login-btn">Zaloguj się</button>
        </form>
        <h4>Nie masz konta?</h4>
        <a href="?page=register"><button type="button" class="register-btn">Zarejestruj się!</button></a>
    </div>
</div>
</body>

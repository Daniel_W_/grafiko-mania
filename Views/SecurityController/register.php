<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/register.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <title>Grafiko-Mania</title>
</head>
<body>
<div class="page">
    <img class="logo" src="Public/img/logo_small.svg" alt="Grafiko-Mania"/>
    <div class="registerbox">
        <form action="?page=register" method="POST">
            <div class = 'messages'>
                <?php
                if(isset($messages))
                    {
                        foreach($messages as $message){
                             echo $message;
                    }
                }
                ?>
            </div>
            <input name="name" type="text" placeholder="Imie">
            <input name="surname" type="text" placeholder="Nazwisko">
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="Hasło">
            <input name="password_repeat" type="password" placeholder="Powtórz hasło">
            <input name="group" type="text" placeholder="Grupa">
            <button type="submit" class="create-btn">Utwórz konto!</button>
        </form>
    </div>
</div>
</body>